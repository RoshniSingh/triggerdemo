# CI/CD Pipeline with Jenkins and GitLab

## Project Overview

This project demonstrates setting up a basic Continuous Integration and Continuous Deployment (CI/CD) pipeline using Jenkins and GitLab. The pipeline automates the build, test, and deployment process of a simple Node.js application.

### Features

- Building and testing a Node.js application
- Deploying the application to a test environment
- Integration with GitLab for triggering pipeline on commits

## Getting Started

Follow the steps below to create and set up the CI/CD pipeline with Jenkins and GitLab:

### Prerequisites

- Git on your local machine
- Node.js and npm(to run app.js need expressjs - npm install express)
- Jenkins (I've used AWS instance to run jenkins)


### Setting Up Jenkins

1. Install Jenkins on your local machine or a server of your choice.
2. Install necessary plugins such as Git, Node.js, etc.
3. Configure Jenkins to integrate with your GitLab repository.


## Steps to Add Webhooks

1. **Access GitLab Project Settings:**
   - Navigate to your project in GitLab.
   - Click on the "Settings" tab in the sidebar.

2. **Select Integrations:**
   - In the left sidebar, select "Integrations" under the "Settings" section.

3. **Add Jenkins Webhook:**
   - Scroll down to the "Jenkins CI / CD" section.
   - Enter the Jenkins webhook URL in the "URL" field. This should be the URL of your Jenkins server's webhook endpoint.
   - Optionally, you can specify a webhook token for additional security.
   - Click on the "Add webhook" button to save the configuration.

4. **Test Webhook:**
   - After adding the webhook, GitLab will send a test request to the Jenkins endpoint.
   - Ensure that Jenkins receives the test request and responds with a successful status code (typically 200 OK).

5. **Trigger Jenkins Jobs:**
   - Once the webhook is configured successfully, GitLab will trigger Jenkins jobs automatically whenever certain events occur in the repository, such as push events.

## Configuring Jenkins Jobs

After setting up the webhook in GitLab, you need to configure Jenkins jobs to handle the webhook requests and perform the desired actions, such as building, testing, and deploying your project. Follow these steps to configure Jenkins jobs:

1. **Create Jenkins Jobs:**
   - In your Jenkins dashboard, create new Jenkins jobs for your project.
   - Configure the jobs to perform the necessary actions (e.g., build, test, deploy) based on the received webhook payloads.

2. **Enable Jenkins Trigger:**
   - Within each Jenkins job configuration, enable the trigger that listens for incoming webhook requests.
   - Configure the trigger settings, such as token verification and job parameters, as needed.

3. **Test Jenkins Integration:**
   - Once the Jenkins jobs are configured, test the integration by pushing code changes to the GitLab repository.
   - Verify that Jenkins receives the webhook requests and executes the corresponding jobs successfully.

