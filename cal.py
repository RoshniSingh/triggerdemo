import numpy as np  # Import NumPy

def rectangle_area(length=5, width=3):
    
    return length * width

def triangle_area(base=4, height=6):
    
    return 0.5 * base * height

def perform_numpy_operations(array):
    
    return {
        "Sum": np.sum(array),
        "Mean": np.mean(array),
        "Max": np.max(array),
        "Min": np.min(array)
    }


print("Length of the rectangle:", 5)
print("Width of the rectangle:", 3)
print("Base of the triangle:", 4)
print("Height of the triangle:", 6)

# Calculating and printing the areas with default values
print("\nAreas with default values:")
print("Area of the rectangle:", rectangle_area())
print("Area of the triangle:", triangle_area())

# Creating a NumPy array
my_array = np.array([1, 2, 3, 4, 5])

# Performing basic NumPy operations on the array
print("\nBasic NumPy operations:")
print("Array:", my_array)
print("Numpy operations:", perform_numpy_operations(my_array))
print("Some changes made")
